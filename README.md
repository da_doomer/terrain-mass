# Terrain Mass

A simple environment to debug control and world-learning algorithms. The
environment has a continuous state space with simple dynamics with
discontinuities.
