"""Tasks in the environment."""
from pathlib import Path
from dataclasses import dataclass
from terrain_mass.environment import EnvironmentInstance
from terrain_mass.parsing import parse
from terrain_mass.parsing import Bounds
from itertools import product
import random


@dataclass
class Task:
    environment: EnvironmentInstance
    target_position: tuple[float, float]
    success_distance_to_target: float
    dt: float
    bounds: Bounds
    waypoints: list[tuple[float, float]]


EXAMPLE_MAP_DIR = Path(__file__).parent/"example_maps"


SUCCESS_DISTANCE_TO_TARGET = 0.2
SAND_DRAG_CONSTANT = 4.0
WATER_DRAG_CONSTANT = 4.0
ACTION_MIN = -1.0
ACTION_MAX = 1.0
DT = 0.1


def generate_map(
    seed: str,
    length: int,
) -> str:
    """Generate a random map string.

    An example of a map string:

    ```
       O   
           
    LLLLLLL
           
    LLLLLLL
       G   
    ```

    In this example, there are two regions of "land". The salamander
    starts at the top of the map. The goal is in the bottom.
    """
    # Generate a top-to-bottom map
    # Make this wide so it's difficult to avoid islands
    # EDIT: I simplified the generation, to only have
    # tall maps with land segments like in the docstring,
    # but this can be adapted to have much more complex
    # patterns later (the rest of the code supports it).
    pieces = list[str]()
    width = 10
    # empty is of the form '      '
    # land is of the form  'LLLLLL'
    empty = "".join([" " for _ in range(width)])
    land = empty.replace(" ", "L")
    pieces.append(
        land+"\n"
    )
    pieces.append(
        empty+"\n"
    )

    start = "O".center(width) + "\n"
    end = "G".center(width) + "\n"

    # Generate map
    _random = random.Random(seed)
    mapstr = start
    for _ in range(length):
        piece = _random.choice(pieces)
        mapstr += piece
    mapstr += end

    return mapstr


def generate_waypoints(
    map_string: str,
    length: int,
    seed: str,
) -> list[tuple[int, int]]:
    """Generate a sequence of waypoints to the goal.

    Consider the following map:

    ```
       O   
           
    LLLLLLL
           
    LLLLLLL
       G   
    ```

    This is a graphical representation of a sequence of waypoints for the map:

    ```
           
       x   
    x      
           
           
        x  
    ```

    This sequence of waypoints would be represented by the tuples

    ```
    (1, 3)
    (2, 0)
    (5, 4)
    ```

    That is, the first entry of each waypoint is it's "vertical" position,
    and the second one its "horizontal" position.
    """
    waypoints = list[tuple[int, int]]()

    # Identify possible waypoints
    lines = map_string.splitlines()
    row_n = len(lines)
    column_n = len(lines[0])
    candidates = list(product(
        list(range(row_n)),
        list(range(column_n)),
    ))

    goal_position: None | tuple[int, int] = None
    initial_position: None | tuple[int, int] = None

    for i, j in candidates:
        if lines[i][j] == "O":
            initial_position = (i, j)
        elif lines[i][j] == "G":
            goal_position = (i, j)

    assert isinstance(goal_position, tuple)
    assert isinstance(initial_position, tuple)

    candidates.remove(goal_position)
    candidates.remove(initial_position)

    # Sample waypoints
    _random = random.Random(seed)
    waypoints = _random.sample(candidates, length)

    return waypoints


def get_example_task(
    map_seed: str,
    waypoint_seed: str,
) -> Task:

    length = 7
    waypoint_n = 2

    # Decide if generated map or handcrafted
    mstr = generate_map(
        map_seed,
        length=length,
    )

    # Generate waypoints
    waypoints = generate_waypoints(
        map_string=mstr,
        length=waypoint_n,
        seed=waypoint_seed,
    )

    # Parse map
    m = parse(
        mstr,
        char_size=SUCCESS_DISTANCE_TO_TARGET,
        waypoints=waypoints,
    )

    environment = EnvironmentInstance(
        sand_drag_constant=SAND_DRAG_CONSTANT,
        water_drag_constant=WATER_DRAG_CONSTANT,
        action_min=ACTION_MIN,
        action_max=ACTION_MAX,
        initial_mass_position=m.initial_position,
        islands=m.islands,
    )
    task = Task(
        environment=environment,
        target_position=m.target_position,
        success_distance_to_target=SUCCESS_DISTANCE_TO_TARGET,
        dt=DT,
        bounds=m.bounds,
        waypoints=m.waypoints,
    )
    return task
