"""Gymnasium wrapper for the environment."""
import gymnasium as gym
from gymnasium import spaces
from terrain_mass.task import get_example_task
from terrain_mass.task import Task
from terrain_mass.environment import EnvironmentInstance
from terrain_mass.parsing import Bounds
from typing import Any
import numpy as np
import random
import torch
from PIL import Image
import tempfile
from pathlib import Path
import numpy.typing as npt
from terrain_mass.plotting import plot_state
from terrain_mass.plotting import get_bounds


OBSERVATION_SCALING = 15.0
REWARD_SCALING = 10


def get_distance_to_target(
        state: torch.Tensor,
        target_position: torch.Tensor,
) -> torch.Tensor:
    position = EnvironmentInstance.get_pos(state)
    distance = (position - target_position).norm()
    assert isinstance(distance, torch.Tensor)
    return distance


def is_in_bounds(
    x_pos: torch.Tensor,
    y_pos: torch.Tensor,
    bounds: Bounds
) -> bool:
    return all((
        x_pos >= bounds.x_min,
        x_pos <= bounds.x_max,
        y_pos >= bounds.y_min,
        y_pos <= bounds.y_max,
    ))


class DenseRewardFunction:
    def __init__(
        self,
        time_penalty: float,
        waypoints: list[tuple[float, float]],
    ):
        self.time_penalty: float = time_penalty
        self.waypoints: list[tuple[float, float]] = waypoints

    def __call__(
        self,
        obs: npt.NDArray[np.float64],
        action: npt.NDArray[np.float64],
        next_obs: npt.NDArray[np.float64],
        t_frac: float,
    ) -> float:
        """- t_frac: number in [0, 1] corresponding to the current
                     fraction of the episode (i.e., `timestep_i/episode_len`).
        """
        # Get system state from observation
        def get_state(obs: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]:
            x = obs[:4]
            x = x
            return x

        x = get_state(obs)
        xn = get_state(next_obs)

        # Identify current target position
        i = min(len(self.waypoints)-1, int(t_frac*len(self.waypoints)))
        target_position = torch.tensor(self.waypoints[i])

        # Compute reward
        distance_prev = get_distance_to_target(
            torch.from_numpy(x),
            target_position,
        )
        distance = get_distance_to_target(
            torch.from_numpy(xn),
            target_position,
        )
        reward = (distance_prev - distance).item()*REWARD_SCALING
        return reward - self.time_penalty


def get_task(seed: str) -> Task:
    """Helper function to get a random task."""
    _random = random.Random(seed)
    return get_example_task(
        map_seed=str(_random.random()),
        waypoint_seed=str(_random.random()),
    )


class TerrainMassEnv(gym.Env[npt.NDArray[np.float64], npt.NDArray[np.float64]]):
    """A specific task instance (i.e., map) of the TerrainMassEnv."""
    metadata = {"render_modes": ["rgb_array"], "render_fps": 60}

    def __init__(
        self,
        render_mode: str | None=None,
        success_reward: float=10.0,
        animation_padding: float=0.5,
        episode_max_steps: int=120,
        plotting_mass_radius: float=0.1,
        max_distance_margin: float=100.0,
        time_penalty: float=0.001,
        seed: str | None=None,
    ):
        if seed is None:
            seed = str(None)

        # Hyperparameters
        self.success_reward: float = success_reward
        self.animation_padding: float = animation_padding
        self.episode_max_steps: int = episode_max_steps
        self.plotting_mass_radius: float = plotting_mass_radius
        self.max_distance_margin: float = max_distance_margin
        self.time_penalty: float = time_penalty

        # State
        self.task: Task = get_task(seed)
        initial_state = self.task.environment.get_initial_state()
        waypoints = self.task.waypoints + [self.task.target_position]
        self.current_state: torch.Tensor = initial_state
        self.episode_steps: int = 0
        self.reward_function = DenseRewardFunction(
            time_penalty=self.time_penalty,
            waypoints=waypoints,
        )
        self.waypoint_status: list[bool] = [False for _ in self.task.waypoints]

        # Reset once to get the size of the observation
        obs, _ = self.reset()
        obs_shape = obs.shape
        self.observation_space: spaces.Space[npt.NDArray[np.float64]] = spaces.Box(
            low=-5.0,
            high=5.0,
            shape=obs_shape,
            dtype=np.float32,
        )
        self.action_space: spaces.Space[npt.NDArray[np.float64]] = spaces.Box(
            low=self.task.environment.action_min,
            high=self.task.environment.action_max,
            shape=(2,),
            dtype=np.float32,
        )

        assert any((
            render_mode is None,
            render_mode in self.metadata["render_modes"],
        ))
        self.render_mode: str | None = render_mode

    def reset_task(self, seed: str):
        self.task = get_task(seed)
        _ = self.reset()

    def _get_observation(self) -> npt.NDArray[np.float64]:
        state = self.current_state
        state = state.detach()
        observation: list[float] = [
            *(state.tolist()),
        ]
        return np.array(observation, dtype=np.float32)

    def _get_terrain_label(self) -> int:
        state = self.current_state
        terrain = self.task.environment.get_terrain(state)
        return terrain.value

    def reset(
        self,
        seed: int | None = None,
        options: Any = None,
    ) -> tuple[npt.NDArray[np.float64], dict[str, Any]]:
        super().reset(seed=seed)

        initial_state = self.task.environment.get_initial_state()

        self.current_state = initial_state
        self.episode_steps = 0
        self.waypoint_status = [False for _ in self.task.waypoints]
        waypoints = self.task.waypoints + [self.task.target_position]
        reward_function = DenseRewardFunction(
            waypoints=waypoints,
            time_penalty=self.time_penalty,
        )
        self.waypoint_status = [False for _ in self.task.waypoints]
        self.reward_function: DenseRewardFunction = reward_function

        observation = self._get_observation()
        info = dict(
            state=self.current_state.tolist(),
            mode=self._get_terrain_label(),
            raw_observation=observation,
        )
        return observation, info

    def _is_terminated(self) -> bool:
        distance = get_distance_to_target(
            self.current_state,
            torch.tensor(self.task.target_position),
        ).item()
        is_terminated = any((
            distance < self.task.success_distance_to_target,
            self.episode_steps > self.episode_max_steps,
        ))
        return is_terminated

    def _is_truncated(self) -> bool:
        is_truncated = any((
            self.episode_steps > self.episode_max_steps,
        ))
        return is_truncated

    def step(self, action: npt.NDArray):
        self.episode_steps += 1
        prev_observation = self._get_observation()
        next_state = self.task.environment.step(
            x=self.current_state,
            action=torch.from_numpy(action),
            dt=self.task.dt,
        )
        self.current_state = next_state
        terminated = self._is_terminated()
        truncated = self._is_truncated()

        # Update target waypoints status
        for i in range(len(self.task.waypoints)):
            waypoint_position = torch.tensor(
                self.task.waypoints[i],
            )
            distance = get_distance_to_target(
                state=next_state,
                target_position=waypoint_position,
            )
            if distance < self.task.success_distance_to_target:
                self.waypoint_status[i] = True
        distance_to_target = get_distance_to_target(
            next_state,
            torch.tensor(self.task.target_position),
        )
        success = all((
            all(self.waypoint_status),
            distance_to_target.item() < self.task.success_distance_to_target,
        ))

        new_observation = self._get_observation()

        t_frac = self.episode_steps/self.episode_max_steps
        reward = self.reward_function(
            obs=prev_observation,
            action=action,
            next_obs=new_observation,
            t_frac=t_frac,
        )

        info = dict(
            state=next_state.tolist(),
            success=success,
            raw_observation=new_observation,
            mode=self._get_terrain_label(),
        )
        return new_observation, reward, terminated, truncated, info

    def render(self):
        if self.render_mode == "rgb_array":
            return self._render_frame()
        raise ValueError(f"Unknown render mode {self.render_mode}!")

    def _render_frame(self) -> npt.NDArray:
        with tempfile.NamedTemporaryFile(suffix=".png") as fp:
            target_position = self.task.target_position
            bounds = get_bounds(
                [self.current_state],
                environment=self.task.environment,
                target_position=target_position,
                waypoints=self.task.waypoints,
            )
            plot_state(
                x=self.current_state,
                x_min=bounds.x_min-self.animation_padding,
                x_max=bounds.x_max+self.animation_padding,
                y_min=bounds.y_min-self.animation_padding,
                y_max=bounds.y_max+self.animation_padding,
                mass_radius=self.plotting_mass_radius,
                environment=self.task.environment,
                target_position=target_position,
                target_position_radius=self.task.success_distance_to_target,
                output_path=Path(fp.name),
                waypoints=self.task.waypoints,
            )
            im = Image.open(fp.name).convert("RGB")
            a = np.asarray(im)
        return a


class MetaTerrainMassEnv(gym.Env[npt.NDArray, npt.NDArray]):
    """A distribution of tasks from the TerrainMassEnv."""
    metadata = {"render_modes": ["rgb_array"], "render_fps": 60}

    def __init__(
        self,
        render_mode=None,
        success_reward: float = 10.0,
        animation_padding: float = 2.0,
        episode_max_steps: int = 300,
        plotting_mass_radius: float = 0.1,
        max_distance_margin: float = 100.0,
        time_penalty: float = 0.001,
        seed: str | None = None,
    ):
        """
        - seeds: terrain seeds from which the task will be sampled.
        - seed: seed used for the task sampler
        """
        if seed is None:
            seed = str(random.random())

        self.seed = seed

        self.render_mode = render_mode
        self.success_reward = success_reward
        self.animation_padding = animation_padding
        self.episode_max_steps = episode_max_steps
        self.plotting_mass_radius = plotting_mass_radius
        self.max_distance_margin = max_distance_margin
        self.time_penalty = time_penalty

        # Instantiate first environment
        task_seed = self.next_task_seed()
        self.current_env = TerrainMassEnv(
            render_mode=render_mode,
            success_reward=success_reward,
            animation_padding=animation_padding,
            episode_max_steps=episode_max_steps,
            plotting_mass_radius=plotting_mass_radius,
            max_distance_margin=max_distance_margin,
            time_penalty=time_penalty,
            seed=task_seed,
        )

        # All tasks share observation and action spaces
        self.observation_space = self.current_env.observation_space
        self.action_space = self.current_env.action_space

    def render(self):
        return self.current_env.render()

    def next_task_seed(self) -> str:
        # Instantiate current seeder
        _random = random.Random(self.seed)

        # Choose task seed
        task_seed = str(_random.random())

        # Update seed
        self.seed = str(_random.random())

        return task_seed

    def reset(self, seed=None, options=None):
        # Move to next task
        task_seed = self.next_task_seed()
        self.current_env = TerrainMassEnv(
            render_mode=self.render_mode,
            success_reward=self.success_reward,
            animation_padding=self.animation_padding,
            episode_max_steps=self.episode_max_steps,
            plotting_mass_radius=self.plotting_mass_radius,
            max_distance_margin=self.max_distance_margin,
            time_penalty=self.time_penalty,
            seed=task_seed,
        )
        return self.current_env.reset()

    def step(self, action):
        return self.current_env.step(action)
