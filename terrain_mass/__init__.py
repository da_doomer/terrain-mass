from gymnasium.envs.registration import register

register(
     id="TerrainMass-v0",
     entry_point="terrain_mass.gymnasium:TerrainMassEnv",
)

register(
     id="MetaTerrainMass-v0",
     entry_point="terrain_mass.gymnasium:MetaTerrainMassEnv",
)
