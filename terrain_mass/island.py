"""Island type defintions."""
from dataclasses import dataclass
import statistics


def dot(a: tuple[float, float], b: tuple[float, float]) -> float:
    return a[0]*b[0] + a[1]*b[1]


def minus(
    a: tuple[float, float],
    b: tuple[float, float]
) -> tuple[float, float]:
    return (b[0]-a[0], b[1]-a[1])


@dataclass
class Rectangle:
    a: tuple[float, float]
    b: tuple[float, float]
    c: tuple[float, float]
    d: tuple[float, float]

    def is_inside(self, x: float, y: float) -> bool:
        # Credit: mathoverflow, Raymond Manzone
        m = (x, y)
        am = minus(self.a, m)
        ab = minus(self.a, self.b)
        bc = minus(self.b, self.c)
        bm = minus(self.b, m)
        return all((
            0 < dot(ab, am) < dot(ab, ab),
            0 < dot(bc, bm) < dot(bc, bc)
        ))

    @property
    def center(self) -> tuple[float, float]:
        xs = [
            self.a[0],
            self.b[0],
            self.c[0],
            self.d[0],
        ]
        ys = [
            self.a[1],
            self.b[1],
            self.c[1],
            self.d[1],
        ]
        x = statistics.mean(xs)
        y = statistics.mean(ys)
        return (x, y)
