"""Parsing of text files into environment instances."""
from dataclasses import dataclass
from terrain_mass.island import Rectangle


@dataclass
class Bounds:
    x_min: float
    x_max: float
    y_min: float
    y_max: float


@dataclass
class Map:
    initial_position: tuple[float, float]
    islands: list[Rectangle]
    target_position: tuple[float, float]
    bounds: Bounds
    waypoints: list[tuple[float, float]]


def parse(
    string: str,
    char_size: float,
    waypoints: list[tuple[int, int]],
) -> Map:
    """Parse a string into a map."""
    initial_position: tuple[float, float] | None = None
    target_position: tuple[float, float] | None = None
    islands = list[Rectangle]()

    def str_to_map(
        i: int | float,
        j: int | float,
    ) -> tuple[float, float]:
        return j*char_size, -i*char_size

    lines = string.splitlines()
    for i, line in enumerate(lines):
        for j, char in enumerate(line):
            if char == " ":
                continue
            if char == "O":
                assert initial_position is None, "Map has more than one mass!"
                initial_position = str_to_map(i+0.5, j+0.5)
            elif char == "L":
                # Turn character into an island
                #
                #    a:(j,-i) ------- d:(j+1, -i)
                #      |                 |
                #      |                 |
                #    b:(j, -(i+1)) ---  c:(j+1, -(i+1))
                #
                #
                a = str_to_map(i, j)
                b = str_to_map(i+1, j)
                c = str_to_map(i+1, j+1)
                d = str_to_map(i, j+1)
                island = Rectangle(a, b, c, d)
                islands.append(island)
            elif char == "G":
                assert target_position is None, "Map has more than one target position!"
                target_position = str_to_map(i+0.5, j+0.5)
            else:
                raise ValueError(f"Map contains unsupported character '{char}'!")

    assert initial_position is not None, "Map string has no mass 'O'!"
    assert target_position is not None, "Map string has no target position 'G'!"

    #for island in islands:
    #    print(island)

    #print("initial position:", initial_position)
    #print("target_position:", target_position)

    # Compute bound values
    x_min = 0
    x_max = len(lines[0])*char_size
    y_min = -(len(lines)+1)*char_size
    y_max = 0
    bounds = Bounds(
        x_min=x_min,
        x_max=x_max,
        y_min=y_min,
        y_max=y_max,
    )

    # Transform waypoints to map coordinates
    map_waypoints = list[tuple[float, float]]()
    for (i, j) in waypoints:
        map_waypoint = str_to_map(i, j)
        map_waypoints.append(map_waypoint)

    m = Map(
        initial_position=initial_position,
        islands=islands,
        target_position=target_position,
        bounds=bounds,
        waypoints=map_waypoints,
    )
    return m
